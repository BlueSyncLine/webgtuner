var GUITAR_STRING_NAMES = [
    "E low", "A", "D", "G", "B", "E high"
];

// Equally-tempered pitches
var GUITAR_ET = [
    82.4068892282,110.0,146.832383959,195.997717991,246.941650628,329.627556913
];

// Justly-intonated pitches
var GUITAR_JI = [
    82.5,110.0,146.666666667,195.555555556,247.5,330.0
];


// This has to be a power of two.
var AUDIO_BUFFER_SIZE = 2048;

// Canvas size.
var CANVAS_WIDTH = 640;
var CANVAS_HEIGHT = 32;


// Update the tuning period.
var tunePeriod;

var audioCtx, sampleRate, audioSource, scriptNode;
var stringSelect, etJiCheckbox, tunePeriodOutput, canvas, render;
var t = 0.0;


function pageLoad() {
    // Create the audio context and a ScriptProcessorNode to capture raw audio.
    audioCtx = new AudioContext();
    sampleRate = audioCtx.sampleRate;
    navigator.mediaDevices.getUserMedia({audio: true}).then(function(stream) {
        audioSource = audioCtx.createMediaStreamSource(stream);
        scriptNode = audioCtx.createScriptProcessor(AUDIO_BUFFER_SIZE, 1, 0);
        scriptNode.onaudioprocess = processAudio;
        audioSource.connect(scriptNode);
    });

    // Create a canvas for rendering the oscilloscope trace.
    canvas = document.createElement("canvas");
    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGHT;
    document.body.appendChild(canvas);
    render = canvas.getContext("2d");
    render.fillStyle = "#808080";

    // Clear the canvas.
    render.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

    // Line break
    document.body.appendChild(document.createElement("br"));

    // String select
    stringSelect = document.createElement("select");

    for (var i = 0; i < GUITAR_STRING_NAMES.length; i++) {
        var stringName = GUITAR_STRING_NAMES[i];
        var option = document.createElement("option");
        option.innerText = stringName;
        option.value = i.toString();
        stringSelect.appendChild(option);
    }
    stringSelect.onchange = updateSelect;
    document.body.appendChild(stringSelect);

    // ET/JI switch
    etJiCheckbox = document.createElement("input");
    etJiCheckbox.type = "checkbox";
    etJiCheckbox.onchange = updateSelect;
    document.body.appendChild(etJiCheckbox);

    // Line break
    document.body.appendChild(document.createElement("br"));

    tunePeriodOutput = document.createElement("output");
    document.body.appendChild(tunePeriodOutput);

    updateSelect();
}

function processAudio(evt) {
    var x;
    var buf = evt.inputBuffer.getChannelData(0);

    for (var i = 0; i < buf.length; i++) {
        t += 1.0;
        // Horizontal sweep.
        if (t >= tunePeriod)
            t -= tunePeriod;

        // Find the X.
        x = t / tunePeriod * CANVAS_WIDTH;
        render.strokeStyle = buf[i] > 0.0 ? "rgba(128, 255, 128, 0.1)" : "rgba(255, 128, 128, 0.1)";

        // Draw a line.
        render.beginPath();
        render.moveTo(x, 0);
        render.lineTo(x, CANVAS_HEIGHT);
        render.stroke();
    }

    // Stroke the drawn path.
    render.stroke();
}

function updateSelect() {
    var tuning = etJiCheckbox.checked ? GUITAR_JI : GUITAR_ET;
    tunePeriod = sampleRate / tuning[parseInt(stringSelect.value)] * 3.0;
    tunePeriodOutput.value = "Period: " + tunePeriod.toFixed(2) + " " + (etJiCheckbox.checked ? "(just intonation)" : "(equal temperament)");
}

pageLoad();
